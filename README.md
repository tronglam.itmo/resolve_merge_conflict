Bài toàn merged conflict xảy ra với kịch bản sau:

- Clone source code từ remote repository, trong source code có file1.txt với nội dung: content 0
```
git clone https://gitlab.com/tronglam.itmo/resolve_merge_conflict.git
Cloning into 'resolve_merge_conflict'...
remote: Enumerating objects: 27, done.
remote: Counting objects: 100% (16/16), done.
remote: Compressing objects: 100% (12/12), done.
remote: Total 27 (delta 1), reused 0 (delta 0), pack-reused 11
Receiving objects: 100% (27/27), 5.92 KiB | 5.92 MiB/s, done.
Resolving deltas: 100% (1/1), done.
➜  Desktop git:(master) ✗ cd resolve_merge_conflict 
➜  resolve_merge_conflict git:(main) cat file1.txt 
content0
```

- Tạo branch1 tại local 
```
➜  resolve_merge_conflict git:(main) git checkout -b branch1 
Switched to a new branch 'branch1'
```
- Tại branch1, ta chỉnh sửa nội dung file1.txt -> content1, sau đó add, commit và push lên remote repo 
```
➜  resolve_merge_conflict git:(branch1) vi file1.txt 
➜  resolve_merge_conflict git:(branch1) ✗ cat file1.txt 
content1
➜  resolve_merge_conflict git:(branch1) ✗ git add . 
➜  resolve_merge_conflict git:(branch1) ✗ git commit -m "add branch 1"
➜  resolve_merge_conflict git:(branch1) git push --set-upstream origin branch1
```
- Tạo branch2 với file1.txt có nội dung là content2, sau đó push lên remote repo tương tự với branch1
- Thực hiện merge branch1 -> main 
- Thực hiện merge branch2 -> main => Xảy ra merge conflict. 
Nguyên nhân: 
- Do 2 nhánh branch1 và branch2 đều được tạo ra từ nhánh main, khi branch1 merge vào main, nó làm thay đổi nội dung của file1.txt so với ban đầu. Sau đó, khi nhánh branch2 merge vào main, nó không tìm thấy nội dung của file1.txt như lúc đầu -> xảy ra merge conflict. 

Cách giải quyết: 
- Trên giao diện, ta chọn button resolve merge conflict, sau đó chọn content tuỳ theo mong muốn. 
